'use strict';

async function getEnero() {
    try {
        const response = await fetch(`https://rickandmortyapi.com/api/episode`);
        const data = await response.json();

        const { results } = data;

        const aa = results.filter((aa) => {
            return aa.air_date.slice(0, 7) === 'January';
        });
        console.log(aa);
    } catch (error) {
        console.log(error);
    }
}

getEnero();

