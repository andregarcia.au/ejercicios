
let seconds = 0;
let minutes = 0;
let hours = 0;
let days = 0;

const myInterval = setInterval(() => {
    seconds++;

    if (seconds === 60) {
        seconds = 0;
        minutes++;
    }

    if (minutes === 60) {
        minutes = 0;
        hours++;
    }

    if (hours === 24) {
        hours = 0;
        days++;
    }
    console.log(
        `Timer started ${days} days, ${hours} hours, ${minutes} minutes and ${seconds} seconds ago`
    );
}, 1000);
